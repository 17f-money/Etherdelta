---
title: The Web App
date: 2017-11-15
---

![Image of Web App][preview-image]
In the bottom left you can see all the available tokens to trade, there's a couple hundred possibilities and sadly no easy way to search for them in the app outside of using control+f (or apple+f on a mac). It's surprising that this functionality is not included as it seems like it should be a rather simple implementation though there must be something I'm overlooking. Also included for each token is it's daily transactions and the lowest bid and offer prices in Ether for the token.

The upper left allows for withdrawing, depositing and transferring both ethereum and individual tokens. This is fairly self-explanatory but you do need to connect an existing Ethereum wallet to be able to do this.

The order book section displays all current orders. Ones in green allow you to sell the specified token for Ethereum and ones in red allow you to but the specified token for Ethereum. Simply click on the order and then and hit sell or buy to complete the order. One issue that may be encountered though is that the website can be very slow to update so it's possible orders will show up that have already been completed.

Below the order book is the New Order section which allows the user to place either a buy or sell order. You specify how much of a token you want to buy or sell and then the price you're willing to accept or pay for it before specifying how long you want the order to stay up for before expiring. 

The chart in the middle provide basic information on prices over time and the current buy/sell prices of the selected token. Below that is a  history of your transactions and important information from EtherDelta. In the upper right is past trades being made with the prices sold at and below that is a twitter feed from EtherDelta. 

[preview-image]: /images/webapp.JPG