---
title: How do EtherDelta Fees Work?
date: 2017-11-15
---


There's two types of fees, the first is for withdrawing and depositing ethereum or tokens. Because these are on-chain transactions, a gas fee is charged. If you are using a wallet called Meta Mask, EtherDelta lets you change the gas fee from the default 4 gwei. A higher fee will allow the transactions to happen faster while a lower fee will slow it down. For anyone not using Meta Mask, the gas fee is a static 4 gwei. 

On top of this fee, for anyone executing a transaction, a 0.3% fee is charged. If you're buying a token, the fee is charged in ether, if you're selling a token the fee is charged in the token you're selling. Also, the gas fee is still charged on top of the 0.3%. The downside with this is that if you go to execute an order and someone has beaten you to it due to slow loading times, you are still charged the gas fee, though not the 0.3% fee on top of that. 

One of the positives about EtherDelta is that placing orders happens off-chain so there is no fee involved. If you want to cancel them, the gas fee applies, but sending an order is 100% fee.

