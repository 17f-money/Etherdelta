---
title: Arbitrage Opportunities
date: 2017-11-15
---


With how EtherDelta is functioned, you can either place orders or execute already placed orders. But, there's no default functionality for connecting matching orders. For instance, if Alice wants to sell 1 OMG for 0.5 ETH and places an order, but I don't see it and place my own order looking to buy 1 OMG for 0.5 ETH, we won't be automatically connected like on some other platforms. This means that if Alica was instead selling her 1 OMG for 0.4 ETH, someone could come in, buy her 1 OMG for 0.4 ETH and then sell it to me for 0.5 ETH and make a profit of 0.1 ETH. 

Because you can only trade between tokens and Ethereum, there's no opportunities to chain between tokens, but it's hypothesized that you could incorporate a website like bittrex into this and calculate differences in price between the two sites and exploit that in chaining opportunities.